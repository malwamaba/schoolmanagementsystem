import java.util.ArrayList;
import java.util.List;

public class School {

    private List<Student> students;
    private List<Teacher> teachers;
    private int totalMoneyEarned;

    private int totalMoneySpent;



    private int totalTeachersSalary;


    /**
     *
     * @param student
     * @param teacher
     */

    public School(List<Student> student, List<Teacher> teacher) {
        this.students = student;
        this.teachers= teacher;
        this.totalMoneyEarned = 0;
        this.totalMoneySpent = 0;
        this.totalTeachersSalary = 0;
    }


    public List<Student> getStudent() {
        return this.students;
    }

    public void addStudent(Student student) {
        this.students.add(student);
    }

    public List<Teacher> getTeacher() {
        return this.teachers;
    }

    public void addTeacher(Teacher teacher) {
        this.teachers.add(teacher);
    }

    public int getTotalMoneyEarned() {
        return this.totalMoneyEarned;
    }

    public void updateTotalMoneyEarned(int MoneyEarned) {
        this.totalMoneyEarned += MoneyEarned;
    }

    public int getTotalMoneySpent() {
        return this.totalMoneySpent;
    }

    public void updateTotalMoneySpent(int MoneySpent) {

        this.totalMoneySpent += MoneySpent;
    }


//    public int getTotalTeachersSalary() {
//        return totalTeachersSalary;
//    }
//
//    public void setTotalTeachersSalary(int totalTeachersSalary) {
//        this.totalTeachersSalary = totalTeachersSalary;
//    }
}
