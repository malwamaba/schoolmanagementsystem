/**
 *
 */

public class Student {
    private int id;
    private String name;
    private int grade;
    private int feesPaid;
    private int feesTotal;


    /**
     *
     * @param id
     * @param name
     * @param grade
     */

    public Student(int id, String name, int grade) {
        this.id = id;
        this.name = name;
        this.grade = grade;
    }



    public void setGrade(int grade) {
        this.grade = grade;
    }


    public void updateFees(int fees) {
        this.feesPaid += fees;
    }

    public void updateFeesTotal(int fees) {
        this.feesTotal -= fees;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getGrade() {
        return grade;
    }
}
